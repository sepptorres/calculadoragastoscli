# CalculadoraGastosCLI



## CLI Calculadora de Gastos
Pequeño proyecto con docker para ejecutar un script de realiza el calculo de gasto y exporta a JSON

## Description
Este proyecto presenta una pequeña aplicación de calculadora de gastos desarrollada en Node.js, que utiliza Docker para facilitar la ejecución del script y la gestión de las dependencias. La aplicación permite al usuario ingresar gastos, calcular el total y exportar los registros a un archivo JSON.

## Estructura del Proyecto:

### Código Fuente:

La lógica de la aplicación se encuentra en un script principal llamado index.js.
Se utilizan módulos de Node.js para organizar el código y mejorar la modularidad.
Gestión de Dependencias:

Se utiliza npm para gestionar las dependencias de la aplicación.
El archivo package.json contiene la información sobre las dependencias y scripts.
Dockerfile:

El Dockerfile especifica cómo construir la imagen de Docker para la aplicación.
Utiliza una imagen base de Node.js y configura el directorio de trabajo, instala dependencias y establece el comando predeterminado para ejecutar el script.
Interacción del Usuario:

La aplicación utiliza la biblioteca inquirer para interactuar con el usuario mediante la línea de comandos.
Permite al usuario ingresar gastos, calcular el total y exportar los registros a un archivo JSON.
Persistencia de Datos:

Se utiliza un directorio /calculadoraGastosCLI/data para almacenar el archivo JSON que contiene los registros de gastos.
El archivo se actualiza cada vez que el usuario decide salir de la aplicación.
Instrucciones para Ejecutar:

### Construcción de la Imagen:

Ejecutar 
``` 
docker build -t calculadora-gastos . 
```
para construir la imagen de Docker.
Ejecución del Contenedor:

Ejecutar 
``` 
docker run -it --rm calculadora-gastos 
```
para iniciar la aplicación en un contenedor Docker interactivo.
Interacción con la Aplicación:

Ingresar los gastos según las indicaciones del script.
Escribir "exit" para finalizar la entrada de gastos y exportar los registros a un archivo JSON.
El archivo JSON estará disponible en el directorio /calculadoraGastosCLI/data del contenedor.
Este pequeño proyecto proporciona una manera eficiente y portable de ejecutar la calculadora de gastos, encapsulando la aplicación y sus dependencias en un contenedor Docker independiente.


## Instalación
### Instrucciones para Ejecutar:

Construcción de la Imagen:

Ejecutar
``` 
 docker build -t calculadora-gastos . 
```
para construir la imagen de Docker.
Ejecución del Contenedor:

Ejecutar 
``` 
docker run -it --rm calculadora-gastos 
``` 
para iniciar la aplicación en un contenedor Docker interactivo.
Interacción con la Aplicación:

Ingresar los gastos según las indicaciones del script.
Escribir "exit" para finalizar la entrada de gastos y exportar los registros a un archivo JSON.
El archivo JSON estará disponible en el directorio /calculadoraGastosCLI/data del contenedor.

### Información GitLab

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sepptorres/calculadoragastoscli.git
git branch -M main
git push -uf origin main
```


## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.