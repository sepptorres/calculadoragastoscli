const fs = require('fs').promises;
const inquirer = require('inquirer');

// Función para calcular el total de gastos
function calcularTotalGastos(gastos) {
  //return gastos.reduce((total, gasto) => total + gasto.monto, 0);
  const total = gastos.reduce((suma, gasto) => suma + Number(gasto.monto), 0);
  return total;
}

// Función principal
async function main() {
  console.log('Bienvenido a la Calculadora de Gastos');

  const gastos = [];

  // Preguntar al usuario por los gastos hasta que decida salir
  while (true) {
    const respuesta = await inquirer.prompt([
      {
        type: 'input',
        name: 'concepto',
        message: 'Ingrese el concepto del gasto (o "exit" para salir):',
      },
      {
        type: 'number',
        name: 'monto',
        message: 'Ingrese el monto del gasto:',
      },
    ]);

    // Salir si el usuario ingresa "exit"
    if (respuesta.concepto.toLowerCase() === 'exit') {
      // Guardar los registros en un archivo JSON
      const jsonGastos = JSON.stringify(gastos, null, 2);
      await fs.writeFile('registros.json', jsonGastos, 'utf-8');

      console.log('Registros guardados en registros.json. ¡Hasta luego!');
      break;
    }

    // Agregar el gasto a la lista
    gastos.push({
      concepto: respuesta.concepto,
      monto: respuesta.monto,
    });
  }

  // Calcular el total de gastos
  const totalGastos = calcularTotalGastos(gastos);

  // Mostrar resultados
  console.log('\nResumen de Gastos:');
  gastos.forEach((gasto) => {
    console.log(`${gasto.concepto}: $${gasto.monto}`);
  });
  console.log(`\nTotal de Gastos: $${totalGastos}`);
}


main();


