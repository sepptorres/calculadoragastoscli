# Imagen Node.js como imagen base
FROM node:20.10.0

# Establece el directorio de trabajo en /calculadoraGastosCLI
WORKDIR /calculadoraGastosCLI

# Crea un directorio llamado /calculadoraGastosCLI/data
RUN mkdir -p /calculadoraGastosCLI/data

# Copia package.json y package-lock.json al directorio de trabajo
COPY calculadoraGastosCLI/package*.json ./

# Instala las dependencias especificadas en package.json
RUN npm install -g npm@latest
RUN npm install inquirer@3.3.0

# Copia los archivos locales de script al directorio de trabajo
COPY calculadoraGastosCLI ./

# Ejecuta index.js cuando se inicie el contenedor
CMD ["node", "index.js"]

